const UUId = require('crypto')
const uuidMiddleware = (req, res, next) => {

  res.setHeader('Content-Type', 'application/json')

  const uuid4 = UUId.randomUUID()
  res.status(200).json({
    "uuid": uuid4
  })
}

module.exports = uuidMiddleware