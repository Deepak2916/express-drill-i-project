const delayMiddleware = (req, res, next) => {

  const seconds = +req.params.seconds
  if (typeof seconds === 'number' && seconds >= 0) {

    setTimeout(() => {

      res.json(
        {
          "second": seconds,
          "message": `delayed ${seconds} seconds`
        }).end()
    }, seconds * 1000)
  } else {

    next({
      message: "please enter valid seconds(>=0)",
      statusCode: 400
    })
  }
}

module.exports = delayMiddleware