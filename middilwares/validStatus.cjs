const http = require('http')

const validStatusMiddleware = (req, res, next) => {

    const status = +req.params.status_code
    const statusCode = http.STATUS_CODES[status]

    if (http.STATUS_CODES[status]) {


        const output =
        {
            "status code": status,
            "message": statusCode
        }
        res.status(status).json(output)

    } else {

        next({
            message: "please enter valid status code",
            statusCode: 400
        })


    }

}

module.exports = validStatusMiddleware