
const homePageMiddleWare = (req, res, next) => {
  
  const url = req.url
  let message;
  let statusCode;
  let color;
  if (url === '/') {
    statusCode = 200
    message = 'HOME PAGE'
    color = 'color:green'
  } 
  else {
    statusCode = 400
    message = 'Invalid end point choose one of the below end points'
    color = 'color:red'
  }

  res.status(statusCode).send(`<!DOCTYPE html>
        <html>
          <head>
          </head>
          <body style="text-align: center;">
              <h1 style=${color}>${message}</h1></h3><br>
              <h3><a href='/html'> - /html</a></h3>
              <br>
              <h3><a href='/json'> - /json </a></h3>
              <br>
              <h3><a href='/uuid'> - /uuid</a></h3>
              <br>
              <h3><a href='/status'> - /status/{status-code}</a></h3>
              <br>
              <h3><a href='/delay/0'> - /delay/{delay_in_seconds}</a></h3>
        
          </body>
        </html>`)
}

module.exports = homePageMiddleWare