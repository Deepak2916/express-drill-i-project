
const htmlPageMiddleware = (req, res, next) => {

  res.status(200).send(`<!DOCTYPE html>
          <html>
            <head>
            </head>
            <body>
                <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
                <p> - Martin Fowler</p>
          
            </body>
          </html>`).end()

}

module.exports = htmlPageMiddleware