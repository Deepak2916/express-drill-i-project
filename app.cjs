const express = require('express')


const app = express()

app.use(express.json())
app.use(express.urlencoded({ extended: false }))

const { PORT } = require('./config')

const requestId = require('express-request-id')

const router = express.Router()

const homePageAndErrorMiddleware = require('./middilwares/homePageAndErrorPage.cjs')
const htmlMiddleware = require('./middilwares/html.cjs')
const jsonMiddleware = require('./middilwares/json.cjs')
const uuidMiddleware = require('./middilwares/uuid.cjs')
const statusMiddleware = require('./middilwares/status.cjs')
const ValidstatusMiddleware = require('./middilwares/validStatus.cjs')
const delayMiddleware = require('./middilwares/delay.cjs')
const logMiddleware = require('./middilwares/log.cjs')
const showLogsMiddleware = require('./middilwares/showLogs.cjs')
const errorHandlingMiddleware = require('./middilwares/errorHandling.cjs')

app.use(requestId())

router.use(logMiddleware)



router.get('/html', htmlMiddleware)
router.get('/json', jsonMiddleware)
router.get('/uuid', uuidMiddleware)
router.get('/status', statusMiddleware)
router.get('/status/:status_code', ValidstatusMiddleware)
router.get('/delay/:seconds', delayMiddleware)
router.get('/log', showLogsMiddleware)
router.use(homePageAndErrorMiddleware)
router.use(errorHandlingMiddleware)


app.use(router)

app.listen(PORT, () => {
    console.log(`server is running, url is http://localhost:${PORT}`)
})